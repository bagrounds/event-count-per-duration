;(function () {
  'use strict'

  /* imports */
  var _ = require('lodash')

  module.exports = eventCountPerDuration

  function eventCountPerDuration (duration) {
    return function eventCount (options) {
      var events = options.events
      var eventValues = events.map(function (event) {
        return event[options.values]
      })

      var windows = generateWindows(eventValues, duration)

      var groups = _.groupBy(events, options.groupBy)


      var result = Object.keys(groups).map(function (group) {
        var groupEvents = events.filter(function (event) {
          return event[options.groupBy] === group
        })

        var counts = windows.map(function (window) {
          var result = {
            start: window.start,
            end: window.end
          }

          var theseEvents = groupEvents.filter(function (event) {
            return result.start < event[options.values] &&
              event[options.values] <= result.end
          })

          result.count = theseEvents.length

          return result
        })

        if (events[0][options.groupBy] === group) {
          counts[0].count += 1
        }

        var result = {}
        result.key = group
        result.values = counts

        return result
      })

      return result
    }
  }

  function generateWindows (events, duration) {
    var start = Math.min.apply(null, events)
    var end = Math.max.apply(null, events)

    if (end === start) {
      end += duration
    }

    var length = Math.ceil((end - start) / duration)

    var result = Array.apply(null, Array(length)).map(function (x, i) {
      var s = start + i * duration
      var e = s + duration
      var x = {
        start: s,
        end: e
      }
      return x
    })

    return result
  }
})()

