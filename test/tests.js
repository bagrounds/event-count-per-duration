;(function () {
  'use strict'

  /* imports */
  var funTest = require('fun-test')
  var funAssert = require('fun-assert')
  var VError = require('verror')

  /* exports */
  module.exports = [
    test1(),
    test2(),
    test3()
  ]

  function test1 () {
    var test = funTest({
      input: 10,
      verifier: function verifier (error, result) {
        if (error) {
          throw new Error('Should not throw error')
        }

        funAssert.type('Function')(result)
      },
      sync: true
    })

    test.description = 'Should return a function'

    return test
  }

  function test2 () {
    var duration = 9

    var events = [
      {
        label: 'a',
        value: 3
      },
      {
        label: 'b',
        value: 5
      },
      {
        label: 'a',
        value: 15
      },
      {
        label: 'a',
        value: 42
      },
      {
        label: 'b',
        value: 44
      },
      {
        label: 'a',
        value: 46
      }
    ]

    var testInput = {
      events: events,
      groupBy: 'label',
      values: 'value'
    }

    var expectedOutput = [
      {
        key: 'a',
        values: [
          {
            start: 3,
            end: 12,
            count: 1
          },
          {
            start: 12,
            end: 21,
            count: 1
          },
          {
            start: 21,
            end: 30,
            count: 0
          },
          {
            start: 30,
            end: 39,
            count: 0
          },
          {
            start: 39,
            end: 48,
            count: 2
          }
        ]
      },
      {
        key: 'b',
        values: [
          {
            start: 3,
            end: 12,
            count: 1
          },
          {
            start: 12,
            end: 21,
            count: 0
          },
          {
            start: 21,
            end: 30,
            count: 0
          },
          {
            start: 30,
            end: 39,
            count: 0
          },
          {
            start: 39,
            end: 48,
            count: 1
          }
        ]
      }
    ]

    var test = funTest({
      input: testInput,
      verifier: function verifier (error, result) {
        if (error) {
          throw new VError(error, 'Should not throw error')
        }

        var type = '[{key: String, values:' +
          ' [{start: Number, end: Number, count: Number}]}]'
        funAssert.type(type)(result)
      },
      transformer: transformerN(duration),
      sync: true
    })

    test.description = 'Should return correct type'

    return test
  }

  function test3 () {
    var duration = 9

    var events = [
      {
        label: 'a',
        value: 3
      },
      {
        label: 'b',
        value: 5
      },
      {
        label: 'a',
        value: 15
      },
      {
        label: 'a',
        value: 42
      },
      {
        label: 'b',
        value: 44
      },
      {
        label: 'a',
        value: 46
      }
    ]

    var testInput = {
      events: events,
      groupBy: 'label',
      values: 'value'
    }

    var expectedOutput = [
      {
        key: 'a',
        values: [
          {
            start: 3,
            end: 12,
            count: 1
          },
          {
            start: 12,
            end: 21,
            count: 1
          },
          {
            start: 21,
            end: 30,
            count: 0
          },
          {
            start: 30,
            end: 39,
            count: 0
          },
          {
            start: 39,
            end: 48,
            count: 2
          }
        ]
      },
      {
        key: 'b',
        values: [
          {
            start: 3,
            end: 12,
            count: 1
          },
          {
            start: 12,
            end: 21,
            count: 0
          },
          {
            start: 21,
            end: 30,
            count: 0
          },
          {
            start: 30,
            end: 39,
            count: 0
          },
          {
            start: 39,
            end: 48,
            count: 1
          }
        ]
      }
    ]

    var test = funTest({
      input: testInput,
      verifier: function verifier (error, result) {
        if (error) {
          throw new Error('Should not throw error')
        }

        funAssert.equal(expectedOutput)(result)
      },
      transformer: transformerN(duration),
      sync: true
    })

    test.description = 'should count events'

    return test
  }

  function transformerN (n) {
    return function (sut) {
      return sut(n)
    }
  }
})()

